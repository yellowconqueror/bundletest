package com.yellowconqueror.bundletest.actor

import akka.actor.Actor
import com.yellowconqueror.bundletest.message._
import com.yellowconqueror.bundletest.util.BundleManager

import scala.collection.mutable

/**
 * Created by shuang on 12/28/15.
 *
 * This actor determines if and how many times a bundle can be applied
 * to a given shopping cart and then returns a BundleAssignment message with the
 * cost when the bundle can be applied or -1 if it can't
 */
class BundleApplier extends Actor{
  /**
   * main receive function that receives messages from other actors
   * @return
   */
  def receive = {
    case BundleAssignment(bundle,cart) =>
      sender ! BundleAssignmentResult(bundle,calculateAppliedBundlePrice(bundle,cart))

  }

  /**
   * calls methods to determine if bundle can be applied, how many times and
   * @param bundle
   * @param cart
   * @return
   */
  def calculateAppliedBundlePrice(bundle:Bundle,cart:Cart):Double = {
    bundle.bundleId match {
      case BundleManager.NO_SPECIALS_BUNDLE.bundleId =>
        cartCost(cart)
      case _ =>
        val bundleFactorCount: BundleFactorCount = retrieveBundleFactorAndReducedCart(bundle, cart)
        bundleFactorCount.factor match {
          case 0 =>
            Double.MaxValue
          case _ =>
            cartCost(bundleFactorCount.cart) + bundleCost(bundle, bundleFactorCount.factor)
        }
      }
  }

  /**
   * makes calls to determine number of times bundle can be applied to cart and removes bundle items from cart
   * @param bundle
   * @param cart
   * @return
   */
  def retrieveBundleFactorAndReducedCart(bundle:Bundle,cart:Cart):BundleFactorCount = {
    val bundleFactor:Int = cartCanApplyBundleNumberOfTimes(0,bundle,cart)
    val reducedCart:Cart = removeBundleItemsFromCart(bundle,cart,bundleFactor)
    BundleFactorCount(reducedCart,bundleFactor)
  }

  /**
   * counts the number of times the bundle can be applied to the cart
   * @param count
   * @param bundle
   * @param cart
   * @return
   */
  def cartCanApplyBundleNumberOfTimes(count:Int,bundle:Bundle,cart:Cart):Int = {
    bundleCanBeAppliedToCart(bundle,cart) match{
      case true =>
        cartCanApplyBundleNumberOfTimes(count+1,bundle,removeBundleItemsFromCart(bundle,cart,1))
      case _ =>
        count
    }
  }

  /**
   * determines whether the bundle can be applied to a given cart
   * @param bundle
   * @param cart
   * @return
   */
  def bundleCanBeAppliedToCart(bundle:Bundle,cart:Cart):Boolean ={
    var result:Boolean = true
    bundle.bundleItems.foreach(bundleItem =>
      if (!cart.cartItems.contains(bundleItem.item) ||
        (cart.cartItems.contains(bundleItem.item) &&
          cart.cartItems.get(bundleItem.item).get < bundle.itemCounts.get(bundleItem.item).get)) {
        result = false
      }
    )
    result
  }

  /**
   * removes bundle items from cart
   * @param bundle
   * @param cart
   * @return
   */
  def removeBundleItemsFromCart(bundle:Bundle,cart:Cart, factor:Int):Cart = {
    val newCartItems:mutable.Map[Item,Int] = new mutable.LinkedHashMap[Item,Int]()

    val commonItems:Set[Item] = cart.cartItems.keySet.intersect(bundle.itemCounts.keySet)

    cart.cartItems.foreach{
      case(item,count)=>
        if(!bundle.itemCounts.keySet.contains(item)){
          newCartItems.put(item,count)
        }else{
          val newCount:Int = cart.cartItems.get(item).get - (bundle.itemCounts.get(item).get * factor)
          if(newCount >= 0){
            newCartItems.put(item,newCount)
          }else{
            throw new IllegalArgumentException("not enough "+ item.itemId+" in cart to remove bundled items "+factor+" times")
          }
        }
    }
    val returnValue = Cart(cart.cartId, newCartItems.toMap)
    return returnValue
  }

  /**
   * calculates the cost of the items in the cart
   * @param cart
   * @return
   */
  def cartCost(cart:Cart):Double = {
    var cost:Double = 0

    cart.cartItems.foreach {
      case (item, count) =>
        cost = cost + (item.price * count)
    }
    return cost
  }

  /**
   * calculates the bundle deal cost of items multipled by the number of times the bundle can be applied to the cart
   * @param bundle
   * @param factor
   * @return
   */
  def bundleCost(bundle:Bundle, factor:Int):Double = {
    var cost:Double = 0
    bundle.bundleItems.foreach(bundleItem =>
      cost = cost + (bundleItem.bundleItemPrice*factor)
    )
    return cost
  }
}
