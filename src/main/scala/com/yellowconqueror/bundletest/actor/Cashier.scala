package com.yellowconqueror.bundletest.actor

import akka.actor.{Props, Actor}
import akka.routing.RoundRobinPool
import akka.util.Timeout
import com.yellowconqueror.bundletest.message._
import com.yellowconqueror.bundletest.util.BundleManager
import akka.pattern.ask
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by shuang on 12/28/15.
 *
 * This actor is the master.  It sends BundleAssignment objects to a pool of
 * BundleApplier actors to determine the price when applying a particular bundle.
 * It keeps track of the lowest price and associated bundle as results are returned
 * and returns the result to the sender.
 */
class Cashier(bundleAppliersPoolSize:Int) extends Actor{
  val numberOfBundles:Int = BundleManager.getBundles.size + 1 //+1 for the no bundle option
  var numberOfResultsReturned:Int = 0
  val bundleAppliers = context.actorOf(RoundRobinPool(bundleAppliersPoolSize).props(Props[BundleApplier]), name = "bundleApplierRouter")

  /**
   * main receive method that accepts the incoming CartAssignment message and uses futures to
   * wait for results to return.
   *
   * @return
   */
  def receive = {
    case CartAssignment(cart) =>
      implicit val timeout = Timeout(15 minutes)
      val futureList = Future.traverse(BundleManager.getBundles())(bundle => bundleAppliers.ask(BundleAssignment(bundle,cart))).mapTo[List[BundleAssignmentResult]]
      val bundleAssignmentResults:List[BundleAssignmentResult] = Await.result(futureList, timeout.duration)
      val bestResult:BundleAssignmentResult = bundleAssignmentResults.min(Ordering.by((ar:BundleAssignmentResult) => ar.cartPrice))
      sender ! CartAssignmentResult(bestResult.bundle,bestResult.cartPrice)
  }

}
