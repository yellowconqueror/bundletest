package com.yellowconqueror.bundletest.message
import scala.collection.mutable

/**
 * Created by shuang on 12/28/15.
 *
 * All messages used
 */
sealed trait BundleMessage

case class Item(itemId:String,price:Double) extends BundleMessage

case class BundleItem(item:Item,bundleItemPrice:Double) extends BundleMessage

case class Bundle(bundleId:String = "NoSpecials", bundleItems:Seq[BundleItem]) extends BundleMessage{
  val itemCounts:Map[Item,Int] = {
    val countBuilder:mutable.LinkedHashMap[Item,Int] = new mutable.LinkedHashMap[Item,Int]
    for(bundleItem <- bundleItems) {
      val count = countBuilder.getOrElse(bundleItem.item,0)
      countBuilder(bundleItem.item) = count+1
    }
    countBuilder.toMap
  }
}

case class Cart(cartId:String, cartItems:Map[Item,Int]) extends BundleMessage

case class CartAssignment(cart:Cart) extends BundleMessage

case class CartAssignmentResult(bundle:Bundle, cartPrice:Double)extends BundleMessage

case class BundleAssignment(bundle:Bundle, cart:Cart)extends BundleMessage

case class BundleAssignmentResult(bundle:Bundle, cartPrice:Double)extends BundleMessage

case class BundleFactorCount(cart:Cart,factor:Int) extends BundleMessage