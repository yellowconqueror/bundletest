package com.yellowconqueror.bundletest.service

import akka.actor.{Props, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import com.yellowconqueror.bundletest.actor.Cashier
import com.yellowconqueror.bundletest.message._

import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by shuang on 12/28/15.
 *
 * plain old scala service class used to call actor classes.
 */
class CashierService(bundleApplierPoolSize:Int) {
  protected val system = ActorSystem("CartPriceSystem")
  protected val cashier = system.actorOf(Props(new Cashier(bundleApplierPoolSize)))

  def getPrice(cart:Cart):Double = {
    implicit val timeout = Timeout(15 minutes)
    val future = cashier ? CartAssignment(cart) // enabled by the “ask” import
    val result:CartAssignmentResult = Await.result(future, timeout.duration).asInstanceOf[CartAssignmentResult]
    return result.cartPrice
  }

}
