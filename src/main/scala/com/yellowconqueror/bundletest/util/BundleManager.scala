package com.yellowconqueror.bundletest.util

import com.yellowconqueror.bundletest.message.{Item, BundleItem, Bundle}

/**
 * Created by shuang on 12/28/15.
 *
 * simple singleton object used to store bundles.
 * I did not incorporate any locking since the requirements
 * specified the bundles would be pre-initialized before
 * processing of carts began.  However, it would be trivial
 * to add locking if desired
 */
object BundleManager {
  val NO_SPECIALS_BUNDLE = Bundle("NoSpecials",List())
  var bundles:List[Bundle] = List()

  def setBundles(bundles:List[Bundle]){
    this.bundles = bundles
  }

  def getBundles():List[Bundle] = {
    return bundles :+ NO_SPECIALS_BUNDLE
  }

}
