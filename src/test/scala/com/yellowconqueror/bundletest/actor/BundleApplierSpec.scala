package com.yellowconqueror.bundletest.actor

import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.yellowconqueror.bundletest.message._
import org.scalatest._

import scala.concurrent.Await
import scala.concurrent.duration._
/**
  * Created by scott on 11/8/16.
  */
class BundleApplierSpec extends FlatSpec with Matchers{
  implicit val timeout = Timeout(15 minutes)
  val system = ActorSystem("testCartPriceSystem")

  "A BundleApplier" should "be able to apply a bundle to a cart once" in {
    val bundleApplier = system.actorOf(Props[BundleApplier])
    val future = bundleApplier ? BundleAssignment(
      Bundle("Buy1AppleGetOneHalfPrice",List(BundleItem(Item("apple",1.00),1.00),BundleItem(Item("apple",1.00),.50))),
      Cart("simpleCart",Map(Item("apple",1.00)->2)))
    val result = Await.result(future, timeout.duration).asInstanceOf[BundleAssignmentResult]
    result.cartPrice should be (1.5)
  }

  it should "be able to apply a bundle more than once" in {
    val bundleApplier = system.actorOf(Props[BundleApplier])
    implicit val timeout = Timeout(15 minutes)
    val future = bundleApplier ? BundleAssignment(
      Bundle("Buy1AppleGetOneHalfPrice",List(BundleItem(Item("apple",1.00),1.00),BundleItem(Item("apple",1.00),.50))),
      Cart("simpleCart",Map(Item("apple",1.00)->4)))
    val result = Await.result(future, timeout.duration).asInstanceOf[BundleAssignmentResult]
    result.cartPrice should be (3)

  }

  it should "be able to apply the bundle and add leftovers to price" in {
    val bundleApplier = system.actorOf(Props[BundleApplier])
    implicit val timeout = Timeout(15 minutes)
    val future = bundleApplier ? BundleAssignment(
      Bundle("Buy1AppleGetOneHalfPrice",List(BundleItem(Item("apple",1.00),1.00),BundleItem(Item("apple",1.00),.50))),
      Cart("simpleCart",Map(Item("apple",1.00)->5)))
    val result = Await.result(future, timeout.duration).asInstanceOf[BundleAssignmentResult]
    result.cartPrice should be (4)
  }
  it should "be able to apply the bundle zero times" in {
    val bundleApplier = system.actorOf(Props[BundleApplier])
    implicit val timeout = Timeout(15 minutes)
    val future = bundleApplier ? BundleAssignment(
      Bundle("Buy1AppleGetOneHalfPrice",List(BundleItem(Item("apple",1.00),1.00),BundleItem(Item("apple",1.00),.50))),
      Cart("simpleCart",Map(Item("orange",1.00)->5)))
    val result = Await.result(future, timeout.duration).asInstanceOf[BundleAssignmentResult]
    result.cartPrice should be (Double.MaxValue)
  }

}
