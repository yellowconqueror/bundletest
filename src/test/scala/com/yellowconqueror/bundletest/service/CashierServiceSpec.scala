package com.yellowconqueror.bundletest.service

import com.yellowconqueror.bundletest.message._
import com.yellowconqueror.bundletest.util.BundleManager
import org.scalatest._

/**
  * Created by scott on 11/8/16.
  */
class CashierServiceSpec extends FlatSpec with Matchers{
  val cashierService:CashierService = new CashierService(5)

  "the CashierService" should "apply the bundle once" in {
    BundleManager.setBundles(singleBundle)
    val cart:Cart = Cart("simpleCart",Map(Item("apple",1.00)->2))
    var price:Double = cashierService.getPrice(cart)
    assert(price == 1.5)

    BundleManager.setBundles(multipleBundles)
    price = cashierService.getPrice(cart)
    price should be (1)
  }

  it should "apply the bundle twice" in {
    BundleManager.setBundles(singleBundle)
    val cart:Cart = Cart("simpleCart",Map(Item("apple",1.00)->4))
    var price:Double = cashierService.getPrice(cart)
    price should be (3)

    BundleManager.setBundles(multipleBundles)
    price = cashierService.getPrice(cart)
    price should be (2)
  }

  it should "apply the bundle twice with leftovers" in {
    BundleManager.setBundles(singleBundle)
    val cart:Cart = Cart("simpleCart",Map(Item("apple",1.00)->5,Item("carrot",1.00)->2))
    var price:Double = cashierService.getPrice(cart)
    price should be (6)

    BundleManager.setBundles(multipleBundles)
    price = cashierService.getPrice(cart)
    price should be (5)
  }

  it should "not apply the bundle" in {
    BundleManager.setBundles(singleBundle)
    val cart:Cart = Cart("simpleCart",Map(Item("grape",1.00)->4))
    var price:Double = cashierService.getPrice(cart)
    price should be (4)

    BundleManager.setBundles(multipleBundles)
    price = cashierService.getPrice(cart)
    price should be (4)
  }


  def multipleBundles:List[Bundle]={
    return List(
      Bundle("Buy1AppleGetOneHalfPrice",List(BundleItem(Item("apple",1.00),.75),BundleItem(Item("apple",1.00),.75))),
      Bundle("BuyBreadGetButterFree",List(BundleItem(Item("bread",2.00),2.00),BundleItem(Item("butter",1.00),0))),
      Bundle("Buy2OrangesGetOneFree",List(BundleItem(Item("orange",1.00),1.00),BundleItem(Item("orange",1.00),1.00),BundleItem(Item("orange",1.00),0))),
      Bundle("Buy1AppleGetOneFree",List(BundleItem(Item("apple",1.00),.50),BundleItem(Item("apple",1.00),.50)))
    )
  }

  def singleBundle:List[Bundle] ={
    return List(Bundle("Buy1AppleGetOneHalfPrice",List(BundleItem(Item("apple",1.00),1.00),BundleItem(Item("apple",1.00),.50))))
  }
}
