## Apply Bundle to Cart Application

This application allows a set of Bundles (special prices for different product combinations) to be initialized as well as a service class that determines what the best possible price for a given cart of items is based on the configured bundles

This application is built using akka actors and futures.
Akka functionality highlights include using routers and the ask pattern

Follow these steps to get started:

1. Git-clone this repository.

2. Change directory into your clone.

3. Launch SBT:

        $ sbt

4. Compile everything:

        > compile

5. play around with unit tests


## Additional resources
+ [akka actors](http://doc.akka.io/docs/akka/2.4.1/scala/actors.html)
+ [akka futures](http://doc.akka.io/docs/akka/2.4.1/scala/futures.html)